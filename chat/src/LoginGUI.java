public class LoginGUI {
	
	private String username, password;
	
	LoginGUI() {
		System.out.print("Username: ");
		username = ChatClient.INPUT.nextLine();
		
		System.out.print("Password: ");
		password = ChatClient.INPUT.nextLine();
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
}