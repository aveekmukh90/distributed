import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class ChatClient {
	
	public static final Scanner INPUT = new Scanner(System.in);
	private static Registry registry;
	
	
	public static void main(String [] args) throws RemoteException, NotBoundException{
		
		//Get remote object reference
		String host = "localhost";//args[0];
		registry = LocateRegistry.getRegistry(host);
	    Chat h = (Chat) registry.lookup("ChatServer");
		
	    // login window
	    LoginGUI login = new LoginGUI();
	    String username = login.getUsername();
	    String password = login.getPassword();
	    h.register(new User(username,password));
	    System.out.println("Logged in as " + username);
	    
	    new ChatGUI(h,username);
	}
}
