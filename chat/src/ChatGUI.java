import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Scanner;

public class ChatGUI {
		
	private Scanner in = ChatClient.INPUT;
	private Chat chat;
	private String username;
	
	ChatGUI(Chat h, String username) {
		chat = h;
		this.username = username;
		
		listUsers();
		
		System.out.println("Type \"help\" to get information about the available commands.");
		String command;
		do {
			System.out.print("Command: ");
			while (!in.hasNextLine());
			command = in.nextLine().trim();
			switch(command) {
			case "quit": 
				System.out.print("Exiting.");
				in.close();
				break;
			case "sendto":
				sendto();
				break;
			case "receive":
				receive();
				break;
			case "help":
				help();
				break;
			case "listUsers":
				listUsers();
				break;
			case "history":
				history();
				break;
			default:
				System.out.println("Invalid command. Type \"help\" to get information about the available commands.");
			}
		} while (!command.equals("quit"));
		
	}

	private void history() {
		System.out.print("Recipient: ");
		String recipient = in.nextLine().trim();
		
		String s1,s2,msg;
		Scanner history = null;
		try {
			history = new Scanner(new File("history-"+username+".txt"));
			System.out.println("Conversation with "+recipient+": ");
		} catch (FileNotFoundException e) {
			System.out.println("No history to show.");
			return;
		}
		while (history.hasNext()) {
			s1 = history.next();
			s2 = history.next();
			msg = history.nextLine();
			if ((s1.equals(recipient) && s2.equals(username))
					|| (s1.equals(username) && s2.equals(recipient))) 
				System.out.println(s1 + " -> " + s2 + " :: " + msg);
		}
		history.close();
	}

	private void listUsers() {
		// download list of active users
	    List<String> activeUsers;
		try {
			activeUsers = chat.getUserList();
			activeUsers.remove(username);
		    System.out.print("Active users:  ");
		    for(String a: activeUsers) System.out.print(a+"  ");
		    System.out.println();
		} catch (RemoteException e) {
			System.err.println("Error while retrieving user list from server.");
			e.printStackTrace();
		}		
	}

	private void help() {
		System.out.println("Available commands:");
		System.out.println("help  --> print the help");	
		System.out.println("sendto  --> send a message to a user");	
		System.out.println("receive  --> print the messages that have been received");
		System.out.println("listUsers  --> print the list of active users");
		System.out.println("history  --> show the history for a given user");
		System.out.println("quit --> quit the chat");
	}

	private void receive() {
		List<Message> received = null;
		try {
			received = chat.receive(username);
		} catch (RemoteException e) {
			System.err.println("Error while retrieving messages from server.");
			e.printStackTrace();
		}
		if (received.size() == 0) System.out.println("No new message.");
		else {
			PrintWriter history = null;
			try {
				history = new PrintWriter(new BufferedWriter(new FileWriter("history-"+username+".txt",true)));
			} catch (IOException e) {
				e.printStackTrace();
			}
			for(Message m: received) {
				if (history != null) history.println(m.sender + " " + username + " " + m.message_body);
				System.out.println(m);
			}
			history.close();
		}
	}

	private void sendto() {
		System.out.print("Recipient: ");
		String recipient = in.nextLine().trim();
		System.out.print("Message: ");
		String msg = in.nextLine().trim();
		try {
			chat.send(recipient,new Message(username,msg));
			
			PrintWriter history = new PrintWriter(new BufferedWriter(new FileWriter("history-"+username+".txt",true)));
			history.println(username + " " + recipient + " " + msg);
			history.close();
			
			System.out.println("Message sent.");
		} catch (IOException e) {
			System.err.println("Error while sending message to server.");
			e.printStackTrace();
		}
	}
}
