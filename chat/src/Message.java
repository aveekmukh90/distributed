import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	String sender;
	String message_body;
	Date time;
	
	public Message(String sndr, String mb){
		sender = sndr;
		message_body = mb;
		time= new Date();
	}
	
	public String toString(){
	     return sender +" sent: " + message_body + " @"+time.toString();
	}
	//timestamp
}
